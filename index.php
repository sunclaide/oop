<?php
    require('animal.php');
    require('frog.php');
    require('ape.php');
    $animal = new animal("Shaun");
    echo "Name : " . $animal->nama ."<br>";
    echo "Legs : " . $animal->legs ."<br>";
    echo "Cold Blooded : " . $animal->cold_blooded ."<br> <br>";

    $animal = new ape("Kera Sakti");
    echo "Name : " . $animal->nama ."<br>";
    echo "Legs : " . $animal->legs ."<br>";
    echo "Cold Blooded : " . $animal->cold_blooded ."<br>";
    echo "Yell : " . $animal->yell ."<br> <br>";

    $animal = new frog("Buduk");
    echo "Name : " . $animal->nama ."<br>";
    echo "Legs : " . $animal->legs ."<br>";
    echo "Cold Blooded : " . $animal->cold_blooded ."<br>";
    echo "Jump : " . $animal->jump ."<br>";

?>